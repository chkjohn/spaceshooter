-----------------------------------------------------------------------------------------
--
-- File: EnemyBullet.lua
-- Author: Chan Ho Kwan
--
-----------------------------------------------------------------------------------------


local EB = {}

function EB:newGroup()
	local enemyBulletGroup = display.newGroup()
	enemyBulletGroup.anchorChildren = true
	enemyBulletGroup.anchorX = 0.5
	enemyBulletGroup.anchorY = 0.5
	enemyBulletGroup.x = 0
	enemyBulletGroup.y = 0
	
	function enemyBulletGroup:newBullet(imageDir, enemyBulletPower, enemyBulletSpeed, x, y, angle)
		local enemyBullet = display.newImage(imageDir .. 'Lasers/laserRed14.png', true)
		enemyBullet.anchorX = 0.5
		enemyBullet.anchorY = 1
		enemyBullet.x = x
		enemyBullet.y = y
		enemyBullet.damage = enemyBulletPower
		enemyBullet.speed = enemyBulletSpeed
		enemyBullet.angle = angle
		enemyBullet:rotate(-angle)
		
		enemyBullet.collision = onEnemyBulletCollision
		enemyBullet:addEventListener("collision", enemyBullet)
		
		self:insert(enemyBullet)
		
		function enemyBullet:collision(event)
			self:removeEventListener("collision", self)
			enemyBulletGroup:remove(self)
			display:remove(self)
			self:removeSelf()
			self = nil
		end
		
		return enemyBullet
	end
	
	function enemyBulletGroup:setMoveBulletTimer()
		local moveEnemyBullets = function() return self:moveEnemyBullets() end
		local moveEnemyBulletTimer = timer.performWithDelay(2, moveEnemyBullets, -1)
		
		return moveEnemyBulletTimer
	end
	
	function enemyBulletGroup:moveEnemyBullets()
		if self.numChildren and self.pause.isPaused == false then
			for a = self.numChildren,1,-1  do
				local tmp = self[a]
				if (tmp.y < display.contentHeight + 100) then
					tmp.x = tmp.x + tmp.speed * math.sin(tmp.angle*math.pi/180)
					tmp.y = tmp.y + tmp.speed * math.cos(tmp.angle*math.pi/180)
				else
					self.physics.removeBody(tmp)
					self:remove(tmp)
					display:remove(tmp)
				end	
			end
		end
	end
	
	return enemyBulletGroup
end

return EB