-----------------------------------------------------------------------------------------
--
-- File: pauseoverlay.lua
-- Author: Chan Ho Kwan
--
-----------------------------------------------------------------------------------------


local composer = require( "composer" )
local scene = composer.newScene()
local widget = require "widget"
local imageDir = "images/"

local buttonSound = audio.loadSound("SoundEffect/select01.wav")


local function resumeGame()
	audio.play(buttonSound)
	composer.hideOverlay(false, "fade", 800)
end

local function restartGame()
	audio.play(buttonSound)
	composer.gotoScene("reloading", "fade", 800)
end

local function backToStartPage()
	audio.play(buttonSound)
	composer.gotoScene("start", "fade", 800)
end

function catchBackgroundOverlay(event)
	return true 
end


function scene:create( event )
	local screenGroup = self.view

	backgroundOverlay = display.newRect(display.contentCenterX, display.contentCenterY, display.contentWidth, display.contentHeight)
	backgroundOverlay.anchorX = 0.5
	backgroundOverlay.anchorY = 0.5
	backgroundOverlay:setFillColor(0.5, 0.5, 0.5)
	backgroundOverlay.alpha = 0.6
	backgroundOverlay.isHitTestable = true
	
	resume = widget.newButton{
		label="Resume",
		labelColor = { default={255}, over={128} },
		font=native.systemFontBold,
		fontSize=40,
		defaultFile=imageDir.."button.png",
		overFile=imageDir.."button-over.png",
		width=308, height=80,
		onRelease = resumeGame	-- event listener function
	}
	resume.anchorX = 0.5
	resume.anchorY = 0.5
	resume.x = display.contentCenterX
	resume.y = display.contentCenterY - 150
	
	restart = widget.newButton{
		label="Restart",
		labelColor = { default={255}, over={128} },
		font=native.systemFontBold,
		fontSize=40,
		defaultFile=imageDir.."button.png",
		overFile=imageDir.."button-over.png",
		width=308, height=80,
		onRelease = restartGame	-- event listener function
	}
	restart.anchorX = 0.5
	restart.anchorY = 0.5
	restart.x = display.contentCenterX
	restart.y = display.contentCenterY
	
	startPage = widget.newButton{
		label="Start Page",
		labelColor = { default={255}, over={128} },
		font=native.systemFontBold,
		fontSize=40,
		defaultFile=imageDir.."button.png",
		overFile=imageDir.."button-over.png",
		width=308, height=80,
		onRelease = backToStartPage	-- event listener function
	}
	startPage.anchorX = 0.5
	startPage.anchorY = 0.5
	startPage.x = display.contentCenterX
	startPage.y = display.contentCenterY + 150
	
	
	screenGroup:insert(backgroundOverlay)
	screenGroup:insert(resume)
	screenGroup:insert(restart)
	screenGroup:insert(startPage)
end


function scene:show( event )
	local phase = event.phase
	if phase == "did" then
		--composer.purgeScene("reloading")
		--composer.purgeScene("start")

		backgroundOverlay:addEventListener ("tap", catchBackgroundOverlay)
		backgroundOverlay:addEventListener ("touch", catchBackgroundOverlay)
	end
end


function scene:hide( event )
	local phase = event.phase
	if phase == "will" then
		backgroundOverlay:removeEventListener ("tap", catchBackgroundOverlay)
		backgroundOverlay:removeEventListener ("touch", catchBackgroundOverlay)
		
		event.parent:resumeGame()
		--audio.dispose(buttonSound)
	end
end


function scene:destroy( event )

end


scene:addEventListener( "create", scene )
scene:addEventListener( "show", scene )
scene:addEventListener( "hide", scene )
scene:addEventListener( "destroy", scene )

return scene