-----------------------------------------------------------------------------------------
--
-- File: Meteor.lua
-- Author: Chan Ho Kwan
--
-----------------------------------------------------------------------------------------


local Meteor = {}

function Meteor:newGroup()
	local meteorGroup = display.newGroup()
	meteorGroup.anchorChildren = true
	meteorGroup.anchorX = 0.5
	meteorGroup.anchorY = 0.5
	meteorGroup.x = 0
	meteorGroup.y = 0
	
	function meteorGroup:newMeteor(imageDir, rand, meteorMaxSpeed, meteorMaxHp, meteorScore)
		local meteor = display.newImageRect(imageDir .. 'Meteors/meteorBrown_big1.png', 101, 84)
		meteor.anchorX = 0.5
		meteor.anchorY = 0.5
		meteor.x = rand(display.contentCenterX - 280, display.contentCenterX + 280)
		meteor.y = -50
		meteor.speed = rand(1, meteorMaxSpeed)
		meteor.hp = meteorMaxHp
		meteor.damage = meteorMaxHp
		meteor.score = meteorScore
		
		meteor:addEventListener("collision", meteor)
		
		meteor.meteorRotation = transition.to( meteor, { rotation=360*rand(-1,1), time=rand(1000, 20000), iterations=1000} )
		
		self:insert(meteor)
		
		function meteor:collision(event)
			if event.phase == "began" then
				self.hp = self.hp - event.other.damage
			end
			
			if self.hp <= 0 then
				self.mydata.score = self.mydata.score + self.score
				self.checkLevelScore()
				self.scoreString.text = string.format("%07d", self.mydata.score)
				self.startExplosion(self)
				if rand(0, self.powerUpChance) == 0 then
					self.addPowerUp(self.x, self.y)
				end
				self:removeEventListener("collision", self)
				transition.cancel(self.meteorRotation)
				meteorGroup:remove(self)
				display:remove(self)
				self:removeSelf()
				self = nil
			end
		end
		
		return meteor
	end
	
	function meteorGroup:setMoveMeteorTimer()
		local moveMeteors = function() return self:moveMeteors() end
		local moveMeteorTimer = timer.performWithDelay(2, moveMeteors, -1)
		
		return moveMeteorTimer
	end
	
	function meteorGroup:pauseRoatation()
		if self.numChildren then
			for a = self.numChildren,1,-1  do
				if(self[a].meteorRotation ~= nil) then
					transition.pause(self[a].meteorRotation)
				end	
			end
		end
	end
	
	function meteorGroup:resumeRoatation()
		if self.numChildren then
			for a = self.numChildren,1,-1  do
				if(self[a].meteorRotation ~= nil) then
					transition.resume(self[a].meteorRotation)
				end	
			end
		end
	end
	
	function meteorGroup:moveMeteors()
		if self.numChildren and self.pause.isPaused == false then
			for a = self.numChildren,1,-1  do
				if(self[a].y < display.contentHeight + 100) then
					self[a].y = self[a].y + self[a].speed
				else
					local tmp = self[a]
					transition.cancel(tmp.meteorRotation)
					self.physics.removeBody(tmp)
					self:remove(tmp)
					display:remove(tmp)
				end	
			end
		end
	end
	
	return meteorGroup
end

return Meteor