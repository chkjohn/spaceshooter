-----------------------------------------------------------------------------------------
--
-- File: Message.lua
-- Author: Chan Ho Kwan
--
-----------------------------------------------------------------------------------------


local Message = {}

function Message:new(imageDir, player)
	local messageString = display.newText("Level " .. player.level, display.contentCenterX, display.contentCenterY - 200, native.systemFontBold, 60, "center")
	messageString:setFillColor(0.5, 1, 1)
	messageString.anchorX = 0.5
	messageString.anchorY = 0.5
	messageString.alpha = 0
	
	function messageString:fadeOut()
		local setNull = function() messageString.fadeOutTransition = nil end
		self.fadeOutTransition = transition.to(self, {time=2000, alpha=0, onComplete=setNull})
	end

	function messageString:fadeIn()
		local messageStringfadeOut = function()
			messageString.fadeInTransition = nil
			self:fadeOut()
		end
		self.fadeInTransition = transition.to(self, {time=2000, alpha=1, onComplete=messageStringfadeOut})
	end
	
	function messageString:pauseTransition()
		if self.fadeInTransition ~= nil then
			transition.pause(self.fadeInTransition)
		end
		if self.fadeOutTransition ~= nil then
			transition.pause(self.fadeOutTransition)
		end
	end
	
	function messageString:resumeTransition()
		if self.fadeInTransition ~= nil then
			transition.resume(self.fadeInTransition)
		end
		if self.fadeOutTransition ~= nil then
			transition.resume(self.fadeOutTransition)
		end
	end
	
	return messageString
end

return Message