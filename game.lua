-----------------------------------------------------------------------------------------
--
-- level1.lua
--
-----------------------------------------------------------------------------------------

local composer = require( "composer" )
local scene = composer.newScene()

-- include Corona's "physics" library
local physics = require "physics"
physics.start()
physics.setGravity( 0, 0 )
physics.pause()

--------------------------------------------

-- forward declarations and other locals
local Player = require ("Player")
local Laser = require ("Laser")
local Message = require ("Message")
local Meteor = require ("Meteor")
local Enemy = require ("Enemy")
local EnemyBullet = require ("EnemyBullet")
local StatusBar = require ("StatusBar")
local PowerUp = require ("PowerUp")
local mydata = require ("mydata")
local imageDir = "images/"

local screenGroup = nil
local background1 = nil
local background2 = nil
local background3 = nil
local playerGroup = nil
local player = nil
local shield = nil
local laserGroup = nil
local enemyBulletGroup = nil
local meteorGroup = nil
local enemyGroup = nil
local powerUpGroup = nil
local statusBar = nil
local damageBar = nil
local playerHpBar = nil
local shieldPowerBar = nil
local scoreString = nil
local pauseButton = nil
local messageString = nil

local playerCollisionFilter = { categoryBits=1, maskBits=102 }
local meteorCollisionFilter = { categoryBits=2, maskBits=11 }
local enemyCollisionFilter = { categoryBits=4, maskBits=9 }
local laserCollisionFilter = { categoryBits=8, maskBits=6 }
local shieldCollisionFilter = { categoryBits=16, maskBits=102 }
local enemyBulletCollisionFilter = { categoryBits=32, maskBits=17 }
local powerUpCollisionFilter = { categoryBits=64, maskBits=17 }

local playerMaxHp = 2000
local shieldMaxPower = 2000
local laserPower = 100
local meteorMaxHp = 400
local enemyMaxHp = 300
local enemyBulletPower = 500

local addLaserTimer = nil
local cleanLaserTimer = nil
local moveEnemyBulletTimer = nil
local addMeteorTimer = nil
local moveMeteorTimer = nil
local addEnemyTimer = nil
local addEnemyTimer2 = nil
local moveEnemyTimer = nil
local movePowerUpTimer = nil
local memTimer = nil
local shieldTransition = nil

local laserTimeInterval = 250
local enemyBulletTimeInterval = 1000
local meteorTimeInterval_min = 5000
local enemyTimeInterval_min = 5000
local meteorTimeInterval_max = 10000
local enemyTimeInterval_max = 10000

local laserSpeed = -1000
local meteorMaxSpeed = 15
local enemyMaxSpeed = 10
local enemyBulletSpeed = 20
local powerUpChance = 2

local SoundTable = {
	buttonSound = audio.loadSound("SoundEffect/select01.wav"),
	laserSound1 = audio.loadSound("SoundEffect/laserfire01.wav"),
	laserSound2 = audio.loadSound("SoundEffect/laserfire02.wav"),
	shieldSound = audio.loadSound("SoundEffect/shieldsound.wav"),
	explosionSound = audio.loadSound("SoundEffect/explosion3.wav"),
	alarmSound = audio.loadSound("SoundEffect/Alarm.wav"),
	powerUpSound = audio.loadSound("SoundEffect/powerup.wav"),
}

local meteorScore = meteorMaxHp/10
local enemyScore = enemyMaxHp/10

local level_score = 500

local e_options = {
	-- Required params
	width = 192,
	height = 192,
	numFrames = 16,
	-- content scaling
	sheetContentWidth = 768,
	sheetContentHeight = 768,
}
local explosionSheet = graphics.newImageSheet(imageDir .. "Effects/explosions.png", e_options)

local rand = math.random
local PI = math.pi
local sin = math.sin
local cos = math.cos

local Pause = {isPaused=false}
local isEnemyAdded = false

math.randomseed(os.time())
mydata.score = 0
local score = 0

local screenW, screenH = display.contentWidth, display.contentHeight
local centerX, centerY = display.contentCenterX, display.contentCenterY


local function scrollBackground(self, event)
	if Pause.isPaused == false then
		if self.y >= screenH + 1425 then
			self.y = screenH - 1425
		else 
			self.y = self.y + self.speed
		end
	end
end

local function onExplosion(self, event)
	if event.phase == "ended" then
		self:removeSelf()
		self = nil
	end
end

local function startExplosion(self)
	audio.play(SoundTable.explosionSound)
	
	local explosion = display.newSprite( explosionSheet, { name="explosions", start=1, count=16, time=1000, loopCount=1 } )
	explosion.anchorX = 0.5
	explosion.anchorY = 0.5
	explosion.x = self.x
    explosion.y = self.y
	explosion:play()
	
	explosion.sprite = onExplosion
	explosion:addEventListener( "sprite", explosion )
	
	screenGroup:insert(explosion)
	explosion:toBack()
	background1:toBack()
	background2:toBack()
end

local function clearTimer()
	if addMeteorTimer ~= nil then
		timer.cancel(addMeteorTimer)
	end
	
	if addEnemyTimer ~= nil then
		timer.cancel(addEnemyTimer)
	end
	
	if addEnemyTimer2 ~= nil then
		timer.cancel(addEnemyTimer2)
	end
end

local function checkLevelScore()
	if player ~= nil then
		if mydata.score >= level_score then
			player.level = player.level + 1
			messageString.text = "Level " .. player.level
			--print(player.level)
			messageString:setFillColor(0.5, 1, 1)
			level_score = level_score*2
			
			meteorMaxHp = meteorMaxHp + 100
			enemyMaxHp = enemyMaxHp + 100
			enemyBulletPower = enemyBulletPower + 100
			
			meteorScore = meteorMaxHp/10
			enemyScore = enemyMaxHp/10
			
			if meteorTimeInterval_min > 1000 then
				meteorTimeInterval_min = meteorTimeInterval_min - 1000
			end
			
			if enemyTimeInterval_min > 2000 then
				enemyTimeInterval_min = enemyTimeInterval_min - 1000
			end
			
			if meteorTimeInterval_max > 2000 then
				meteorTimeInterval_max = meteorTimeInterval_max - 2000
			end
			
			if enemyTimeInterval_max > 4000 then
				enemyTimeInterval_max = enemyTimeInterval_max - 2000
			end
			
			if enemyBulletTimeInterval > 500 then
				enemyBulletTimeInterval = enemyBulletTimeInterval - 100
			end
			
			clearTimer()
			setTimer()
			messageString:fadeIn()
		end
	end
end

local function addPowerUp(x, y)
	local powerUpType = rand(0, 2)
	local powerUp = powerUpGroup:newPowerUp(imageDir, powerUpType, x, y, playerGroup)
	local addBody = function()
		physics.addBody(powerUp, "static", {density=1, bounce=0, friction=.2, filter=powerUpCollisionFilter})
	end
	timer.performWithDelay(50, addBody, 1)
end

local function addMeteors()
	if Pause.isPaused == false then
		local meteor = meteorGroup:newMeteor(imageDir, rand, meteorMaxSpeed, meteorMaxHp, meteorScore)
		physics.addBody(meteor, "static", {density=1, bounce=0, friction=.2, radius=42, filter=meteorCollisionFilter})
		
		meteor.mydata = mydata
		meteor.checkLevelScore = checkLevelScore
		meteor.scoreString = scoreString
		meteor.startExplosion = startExplosion
		meteor.addPowerUp = addPowerUp
		meteor.powerUpChance = powerUpChance
		
		local m_delay = rand(meteorTimeInterval_min, meteorTimeInterval_max)
		addMeteorTimer = timer.performWithDelay(m_delay, addMeteors, 1)
	end
end

local function addEnemyBullets(x, y, level, e_type)
	if Pause.isPaused == false then
		audio.play(SoundTable.laserSound2)
		
		local enemyBullet = enemyBulletGroup:newBullet(imageDir, enemyBulletPower * level, enemyBulletSpeed, x, y + 40, 0)
		physics.addBody(enemyBullet, "static", {density=1, bounce=0, friction=.2, filter=enemyBulletCollisionFilter})
		
		if player.level >= 4 and e_type == 1 then
			local enemyBullet2 = enemyBulletGroup:newBullet(imageDir, enemyBulletPower * level, enemyBulletSpeed, x + 40, y + 40, 20)
			physics.addBody(enemyBullet2, "static", {density=1, bounce=0, friction=.2, filter=enemyBulletCollisionFilter})
			
			local enemyBullet3 = enemyBulletGroup:newBullet(imageDir, enemyBulletPower * level, enemyBulletSpeed, x - 40, y + 40, -20)
			physics.addBody(enemyBullet3, "static", {density=1, bounce=0, friction=.2, filter=enemyBulletCollisionFilter})
		end
	end
end

local function addEnemies()
	if Pause.isPaused == false then
		local enemy = enemyGroup:newEnemy(imageDir, rand, enemyMaxSpeed, enemyMaxHp, enemyScore, 1)
		physics.addBody(enemy, "static", {density=1, bounce=0, friction=.2, radius=42, filter=enemyCollisionFilter})

		enemy.addBullets = function() return addEnemyBullets(enemy.x, enemy.y, 1, 1) end
		enemy.addBulletTimer = timer.performWithDelay(enemyBulletTimeInterval, enemy.addBullets, -1)
		
		enemy.mydata = mydata
		enemy.checkLevelScore = checkLevelScore
		enemy.scoreString = scoreString
		enemy.startExplosion = startExplosion
		enemy.addPowerUp = addPowerUp
		enemy.powerUpChance = powerUpChance
		
		local e_delay = rand(enemyTimeInterval_min, enemyTimeInterval_max)
		addEnemyTimer = timer.performWithDelay(e_delay, addEnemies, 1)
	end
end

local function addEnemies2()
	if Pause.isPaused == false then
		if rand(0, 1) == 1 then
			local enemy = enemyGroup:newEnemy(imageDir, rand, enemyMaxSpeed, enemyMaxHp, enemyScore, player.level)
			physics.addBody(enemy, "static", {density=1, bounce=0, friction=.2, radius=42, filter=enemyCollisionFilter})

			enemy.addBullets = function() return addEnemyBullets(enemy.x, enemy.y, 2, 2) end
			enemy.addBulletTimer = timer.performWithDelay(enemyBulletTimeInterval, enemy.addBullets, -1)
			
			enemy.mydata = mydata
			enemy.checkLevelScore = checkLevelScore
			enemy.scoreString = scoreString
			enemy.startExplosion = startExplosion
			enemy.addPowerUp = addPowerUp
			enemy.powerUpChance = powerUpChance
		end
		
		local e_delay = rand(enemyTimeInterval_min, enemyTimeInterval_max)
		addEnemyTimer2 = timer.performWithDelay(e_delay, addEnemies2, 1)
	end
end

function setTimer()
	local m_delay = rand(meteorTimeInterval_min, meteorTimeInterval_max)
	addMeteorTimer = timer.performWithDelay(m_delay, addMeteors, 1)
	
	local e_delay = rand(enemyTimeInterval_min, enemyTimeInterval_max)
	addEnemyTimer = timer.performWithDelay(e_delay, addEnemies, 1)
	
	
	if player.level >= 4 then
		local e_delay = rand(enemyTimeInterval_min, enemyTimeInterval_max)
		addEnemyTimer2 = timer.performWithDelay(e_delay, addEnemies2, 1)
	end
end

local function addLasers()
	if Pause.isPaused == false then
		audio.play(SoundTable.laserSound1)
		local image, power
		if playerGroup.laserLevel == 1 then
			image = 'Lasers/laserGreen10.png'
			power = laserPower
		elseif playerGroup.laserLevel == 2 then
			image = 'Lasers/laserGreen_double.png'
			power = laserPower * 2
		elseif playerGroup.laserLevel >= 3 then
			image = 'Lasers/laserGreen_triple.png'
			power = laserPower * 3
		end
		
		local laser1 = laserGroup:newLaser(player.x, player.y - 50, power, laserSpeed, imageDir, image)
		physics.addBody(laser1, "dynamic", {density=1, bounce=0, friction=.2, filter=laserCollisionFilter})
		laser1:setLinearVelocity(0, laserSpeed)
		
		if playerGroup.laserLevel >= 4 then
			if playerGroup.laserLevel == 4 then
				image = 'Lasers/laserGreen10.png'
				power = laserPower
			elseif playerGroup.laserLevel == 5 then
				image = 'Lasers/laserGreen_double.png'
				power = laserPower * 2
			elseif playerGroup.laserLevel >= 6 then
				image = 'Lasers/laserGreen_triple.png'
				power = laserPower * 3
			end
			local laser2 = laserGroup:newLaser(player.x - 40, player.y - 40, power, laserSpeed, imageDir, image)
			physics.addBody(laser2, "dynamic", {density=1, bounce=0, friction=.2, filter=laserCollisionFilter})
			laser2:setLinearVelocity(laserSpeed * sin(20*PI/180), laserSpeed * cos(20*PI/180))
			laser2:rotate(-20)
			
			local laser3 = laserGroup:newLaser(player.x + 40, player.y - 40, power, laserSpeed, imageDir, image)
			physics.addBody(laser3, "dynamic", {density=1, bounce=0, friction=.2, filter=laserCollisionFilter})
			laser3:setLinearVelocity(laserSpeed * sin(-20*PI/180), laserSpeed * cos(20*PI/180))
			laser3:rotate(20)
		end
	end
end

local function playerReady()
	playerGroup.isReady = true
	playerGroup.laserLevel = 1
	
	playerGroup.playerHpBar = playerHpBar
	playerGroup.shieldPowerBar = shieldPowerBar
	playerGroup.messageString = messageString
	playerGroup.startExplosion = startExplosion
	playerGroup.composer = composer
	playerGroup.shieldSound = SoundTable.shieldSound
	playerGroup.alarmSound = SoundTable.alarmSound
	playerGroup.powerUpSound = SoundTable.powerUpSound
	playerGroup.pause = Pause
	
	player:addEventListener("touch", player)
	
	clearTimer()
	setTimer()
	messageString:fadeIn()
	playerGroup.addLaserTimer = timer.performWithDelay(laserTimeInterval, addLasers, -1)
	timer.pause(playerGroup.addLaserTimer)
	cleanLaserTimer = laserGroup:setCleanLaserTimer()
end

local function onTouch(event)
	if player.hp > 0 and playerGroup.isReady == true and playerGroup.touched == true then
		player:touch(event)
	end
	return true
end

local function onPause(event)
	audio.play(SoundTable.buttonSound)
	Pause.isPaused = true
	Runtime:removeEventListener("enterFrame", background1)
	Runtime:removeEventListener("enterFrame", background2)
	Runtime:removeEventListener("enterFrame", meteorGroup)
	Runtime:removeEventListener("enterFrame", enemyGroup)
	Runtime:removeEventListener("enterFrame", enemyBulletGroup)
	Runtime:removeEventListener("enterFrame", powerUpGroup)
	physics.pause()
	clearTimer()
	meteorGroup:pauseRoatation()
	messageString:pauseTransition()
	composer.showOverlay( "pauseoverlay" ,{ effect = "fade", time = 800, isModal = true} )
	
	return true
end

local function checkMemory()
	collectgarbage( "collect" )
	local memUsage_str = string.format( "MEMORY = %.3f KB", collectgarbage( "count" ) )
	print( memUsage_str, "TEXTURE = "..(system.getInfo("textureMemoryUsed") / (1024 * 1024) ) )
end

local function clearGroup(group)
	if group.numChildren then
		for a = group.numChildren, 1, -1 do
			--physics.removeBody(group[a])
			local tmp = group[a]
			group:remove(tmp)
			tmp = nil
		end
		group:removeSelf()
		group = nil
	end
end

local function setVariableToNULL()
	clearGroup(playerGroup)
	clearGroup(laserGroup)
	clearGroup(enemyBulletGroup)
	clearGroup(meteorGroup)
	clearGroup(enemyGroup)
	clearGroup(powerUpGroup)
	clearGroup(statusBar)
	
	scoreString:removeSelf()
	messageString:removeSelf()
	scoreString = nil
	messageString = nil
end

function scene:resumeGame()
	Pause.isPaused = false
	Runtime:addEventListener("enterFrame", background1)
	Runtime:addEventListener("enterFrame", background2)
	Runtime:addEventListener("enterFrame", meteorGroup)
	Runtime:addEventListener("enterFrame", enemyGroup)
	Runtime:addEventListener("enterFrame", enemyBulletGroup)
	Runtime:addEventListener("enterFrame", powerUpGroup)
	physics.start()
	setTimer()
	meteorGroup:resumeRoatation()
	messageString:resumeTransition()
end

function scene:create( event )

	-- Called when the scene's view does not exist.
	-- 
	-- INSERT code here to initialize the scene
	-- e.g. add display objects to 'sceneGroup', add touch listeners, etc.

	screenGroup = self.view

	-- background 
	background1 = display.newImageRect(imageDir .. "starscape.png", 800, 1425)
	background1.anchorX = 0.5
	background1.anchorY = 1
	background1.x = centerX
	background1.y = screenH
	background1.speed = 2
	
	background2 = display.newImageRect(imageDir .. "starscape.png", 800, 1425)
	background2.anchorX = 0.5
	background2.anchorY = 1
	background2.x = centerX
	background2.y = screenH - 1425
	background2.speed = 2
	
	-- player & shield
	local s_options = {
		-- Required params
		width = 144,
		height = 140,
		numFrames = 3,
		-- content scaling
		sheetContentWidth = 144,
		sheetContentHeight = 420,
	}
	
	playerGroup = Player:newGroup()
	playerGroup.isReady = false
	
	player = playerGroup:newPlayer(imageDir, playerMaxHp)
    physics.addBody(player, "dynamic", {density=1, bounce=1, friction=.2, radius=35, filter=playerCollisionFilter})
	player.isBodyActive = false
	
	shield = playerGroup:newShield(imageDir, shieldMaxPower, s_options)
    physics.addBody(shield, "dynamic", {density=1, bounce=0, friction=.2, radius=70, filter=playerCollisionFilter})
	
	transition.to(player,{time=2000, y=screenH - 100, onComplete=playerReady})
	transition.to(shield,{time=2000, y=screenH - 100, onComplete=playerReady})
	
	-- lasers
	laserGroup = Laser:newGroup()
	laserGroup.physics = physics
	
	-- enemy bullet
	enemyBulletGroup = EnemyBullet:newGroup()
	enemyBulletGroup.physics = physics
	enemyBulletGroup.pause = Pause
	
	-- meteors
	meteorGroup = Meteor:newGroup()
	meteorGroup.physics = physics
	meteorGroup.pause = Pause
	
	-- enemies
	enemyGroup = Enemy:newGroup()
	enemyGroup.physics = physics
	enemyGroup.pause = Pause
	
	-- powerUps
	powerUpGroup = PowerUp:newGroup()
	powerUpGroup.physics = physics
	powerUpGroup.pause = Pause
	
	-- player status bars
	statusBar = StatusBar:newGroup()
	statusBar:toFront()
	playerHpBar = statusBar:newPlayerHpBar(playerMaxHp)
	shieldPowerBar = statusBar:newShieldPowerBar(shieldMaxPower)
	
	-- score text
	score = string.format("%07d", mydata.score)
	scoreString = display.newText(score, screenW, 0, native.systemFontBold, 70, "right")
	scoreString:setFillColor(1, 1, 1)
	scoreString.anchorX = 1
	scoreString.anchorY = 0
	
	-- message text
	messageString = Message:new(imageDir, player)

	-- pause button
	pauseButton = display.newImageRect(imageDir .. "pause.png", 70,70)
	pauseButton.x = centerX
	pauseButton.y = 10
	pauseButton.anchorX = 0.5
	pauseButton.anchorY = 0
	
	
	screenGroup:insert(background1)
	screenGroup:insert(background2)
	screenGroup:insert(playerGroup)
	screenGroup:insert(laserGroup)
	screenGroup:insert(enemyBulletGroup)
	screenGroup:insert(meteorGroup)
	screenGroup:insert(enemyGroup)
	screenGroup:insert(powerUpGroup)
	screenGroup:insert(statusBar)
	screenGroup:insert(scoreString)
	screenGroup:insert(messageString)
	screenGroup:insert(pauseButton)
	
end


function scene:show( event )
	local sceneGroup = self.view
	local phase = event.phase
	
	if phase == "will" then
		-- Called when the scene is still off screen and is about to move on screen
	elseif phase == "did" then
		-- Called when the scene is now on screen
		-- 
		-- INSERT code here to make the scene come alive
		-- e.g. start timers, begin animation, play audio, etc.
		physics.start()
		
		composer.removeScene("start", true)
		composer.removeScene("restart", true)

		background1.enterFrame = scrollBackground
		Runtime:addEventListener("enterFrame", background1)
		
		background2.enterFrame = scrollBackground
		Runtime:addEventListener("enterFrame", background2)
		
		meteorGroup.enterFrame = function() return meteorGroup:moveMeteors() end
		Runtime:addEventListener("enterFrame", meteorGroup)
		
		enemyGroup.enterFrame = function() return enemyGroup:moveEnemies() end
		Runtime:addEventListener("enterFrame", enemyGroup)
		
		enemyBulletGroup.enterFrame = function() return enemyBulletGroup:moveEnemyBullets() end
		Runtime:addEventListener("enterFrame", enemyBulletGroup)
		
		powerUpGroup.enterFrame = function() return powerUpGroup:movePowerUps() end
		Runtime:addEventListener("enterFrame", powerUpGroup)
		
		Runtime:addEventListener("touch", onTouch)
		
		pauseButton:addEventListener("touch", onPause)
		
		--Runtime:addEventListener("system", onSystemEvent)
		
		memTimer = timer.performWithDelay( 1000, checkMemory, 0 )
	end
end

function scene:hide( event )
	local sceneGroup = self.view
	
	local phase = event.phase
	
	if event.phase == "will" then
		-- Called when the scene is on screen and is about to move off screen
		--
		-- INSERT code here to pause the scene
		-- e.g. stop timers, stop animation, unload sounds, etc.)
		Runtime:removeEventListener("enterFrame", background1)
		Runtime:removeEventListener("enterFrame", background2)
		Runtime:removeEventListener("enterFrame", meteorGroup)
		Runtime:removeEventListener("enterFrame", enemyGroup)
		Runtime:removeEventListener("enterFrame", enemyBulletGroup)
		Runtime:removeEventListener("enterFrame", powerUpGroup)
		
		Runtime:removeEventListener("touch", onTouch)
		
		pauseButton:removeEventListener("touch", onPause)
		
		--Runtime:removeEventListener("system", onSystemEvent)
		
		transition.cancel(playerGroup.regenShieldTransition)
		
		for id, value in pairs(timer._runlist) do
			timer.cancel(value)
		end
		
		for s=#SoundTable,1,-1 do
			audio.dispose( SoundTable[s] )
			SoundTable[s] = nil
		end
		
		setVariableToNULL()
		physics.stop()
	elseif phase == "did" then
		-- Called when the scene is now off screen
	end	
	
end

function scene:destroy( event )

	-- Called prior to the removal of scene's "view" (sceneGroup)
	-- 
	-- INSERT code here to cleanup the scene
	-- e.g. remove display objects, remove touch listeners, save state, etc.
	local sceneGroup = self.view
	
	package.loaded[physics] = nil
	physics = nil
end

---------------------------------------------------------------------------------

-- Listener setup
scene:addEventListener( "create", scene )
scene:addEventListener( "show", scene )
scene:addEventListener( "hide", scene )
scene:addEventListener( "destroy", scene )

-----------------------------------------------------------------------------------------

return scene