-----------------------------------------------------------------------------------------
--
-- File: Player.lua
-- Author: Chan Ho Kwan
--
-----------------------------------------------------------------------------------------


local Player = {}

function Player:newGroup()
	local playerGroup = display.newGroup()
    playerGroup.x = 0
    playerGroup.y = 0
	playerGroup.touched = false
	
	function playerGroup:newPlayer(imageDir, playerMaxHp)
		local player = display.newImageRect(imageDir .. "playerShip1_green.png", 99, 75)
		player.anchorX = 0.5
		player.anchorY = 0.5
		player.x = display.contentCenterX
		player.y = display.contentHeight + 200
		player.hp = playerMaxHp
		player.maxHp = playerMaxHp
		player.damage = playerMaxHp*10
		player.level = 1
		
		player:addEventListener("collision", player)
		
		self:insert(player)
		
		function player:touch(event)
			if playerGroup.isReady == true and playerGroup.pause.isPaused == false then
				if event.phase == "began" then
					playerGroup.touched = true
					self.markX = event.x - self.x    -- store x location of object
					self.markY = event.y - self.y    -- store y location of object
					
					if playerGroup.addLaserTimer ~= nil then
						timer.resume(playerGroup.addLaserTimer)
					end
				elseif event.phase == "moved" then
					local x = event.x - self.markX
					local y = event.y - self.markY
					
					if x < 50 then
						x = 50
					elseif x > display.contentWidth - 50 then
						x = display.contentWidth - 50
					end
					
					if y < 25 then
						y = 25
					elseif y > display.contentHeight - 25 then
						y = display.contentHeight - 25
					end
					
					transition.to(self, {time=2, x=x, y=y})
					transition.to(playerGroup[2], {time=2, x=x, y=y})
				elseif event.phase == "ended" then
					playerGroup.touched = false
					if playerGroup.addLaserTimer ~= nil then
						timer.pause(playerGroup.addLaserTimer)
					end
				end
				
				if playerGroup.shieldTransition ~= nil then
					transition.cancel(playerGroup.shieldTransition)
					playerGroup.shieldTransition = nil
				end
			end
			return true
		end
		
		function player:collision(event)
			if event.phase == "began" then
				if event.other.powerUpType == nil then
					self.hp = self.hp - event.other.damage
					if playerGroup.laserLevel > 1 then
						playerGroup.laserLevel = playerGroup.laserLevel - 1
					end
					
					if self.hp <= 0 then
						transition.to(playerGroup.playerHpBar, {time=200, width=0})
						self.isVisible = false
						timer.cancel(playerGroup.addLaserTimer)
						if playerGroup.regenShieldTimer ~= nil then
							timer.cancel(playerGroup.regenShieldTimer)
							playerGroup.regenShieldTimer = nil
						end
						if playerGroup.regenShieldTransition ~= nil then
							timer.cancel(playerGroup.regenShieldTransition)
							playerGroup.regenShieldTransition = nil
						end
						playerGroup.startExplosion(self)
						self.gameOver = function() return playerGroup:gameOver() end
						timer.performWithDelay(2000, self.gameOver, 1)
						playerGroup:remove(self)
						display:remove(self)
						self:removeSelf()
						self = nil
					else
						transition.to(playerGroup.playerHpBar, {time=200, width=self.hp/10})
						if self.hp <= self.maxHp*0.3 then
							audio.play(playerGroup.alarmSound)
							playerGroup.messageString.text = "HP is lower than 30%"
							playerGroup.messageString:setFillColor(1, 0, 0)
							playerGroup.messageString:fadeIn()
						end
						transition.to(self, {time=200, x=self.x, y=self.y})
					end
				end
			end
		end
		
		return player
	end
	
	function playerGroup:newShield(imageDir, shieldMaxPower, s_options)
		local shieldSheet = graphics.newImageSheet(imageDir .. "Effects/shield4.png", s_options)
		local shield = display.newSprite(shieldSheet, { name="shield", start=1, count=2, time=100, loopCount=2, loopDirection = "bounce" })
		shield.anchorX = 0.5
		shield.anchorY = 0.5
		shield.x = display.contentCenterX
		shield.y = display.contentHeight + 200
		shield.power = shieldMaxPower
		shield.maxPower = shieldMaxPower
		shield.damage = shieldMaxPower*10
		shield.isVisible = false
		shield:addEventListener("collision", shield)
		shield:addEventListener("sprite", shield )
		
		self:insert(shield)
		
		function shield:sprite(event)
			if event.phase == "ended" then
				--print "shield deactivated"
				self.isVisible = false
			end
		end
		
		function shield:collision(event)
			if event.phase == "began" then
				if event.other.powerUpType == nil then
					--print "shield activated"
					audio.play(playerGroup.shieldSound)
					self:play()
					self.isVisible = true
					
					self.power = self.power - event.other.damage/2
					if playerGroup.regenShieldTransition ~= nil then
						transition.cancel(playerGroup.regenShieldTransition)
						playerGroup.regenShieldTransition = nil
					end
					--print(self.power)
				
					if self.power <= 0 then
						audio.play(playerGroup.alarmSound)
						transition.to(playerGroup.shieldPowerBar, {time=200, width=0})
						self.power = 0
						self.disableShield = function() return disableShield(self) end
						timer.performWithDelay(200, self.disableShield, 1)
					else
						transition.to(playerGroup.shieldPowerBar, {time=200, width=self.power/10})
					end
				end
				playerGroup.shieldTransition = transition.to(self, {time=20, x=playerGroup[1].x, y=playerGroup[1].y})
			end
		end

		function disableShield(self)
			self.isBodyActive = false
			playerGroup[1].isBodyActive = true
			playerGroup.messageString.text = "Shield is gone"
			playerGroup.messageString:setFillColor(1, 0, 0)
			playerGroup.messageString:fadeIn()
		end
		
		return shield
	end
	
	function playerGroup:gameOver()
		self.composer.gotoScene("restart", "fade", 800)
		--self = nil
	end
	
	return playerGroup
end

return Player