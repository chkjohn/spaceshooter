-----------------------------------------------------------------------------------------
--
-- File: reloading.lua
-- Author: Chan Ho Kwan
--
-----------------------------------------------------------------------------------------


local composer = require( "composer" )
local scene = composer.newScene()

local loadText = nil

local function restartGame (event)
	composer.gotoScene("game", {time=800, effect= "fade"})
end

local function fadeOut()
	transition.to( loadText, {time=800, alpha = 0.0, onComplete=restartGame} )
end

function scene:create( event )
	local screenGroup = self.view
	myStaticgroup = self.view 

	loadText = display.newText("Restarting", display.contentCenterX, display.contentCenterY, native.systemFontBold, 70)
	loadText.anchorX = 0.5
	loadText.anchorY = 0.5
	loadText.alpha = 0.0
	screenGroup:insert(loadText)

end 


function scene:show( event )
	local phase = event.phase
	
	if phase == "did" then
		composer.removeScene("game")
		loadText.alpha = 1.0
		transition.to( loadText, {time=800, alpha = 1.0, onComplete=fadeOut} )
	end
end 


function scene:hide( event )
	
end


function scene:destroy( event )

end


scene:addEventListener( "create", scene )
scene:addEventListener( "show", scene )
scene:addEventListener( "hide", scene )
scene:addEventListener( "destroy", scene )


return scene

