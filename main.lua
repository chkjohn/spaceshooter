-----------------------------------------------------------------------------------------
--
-- File:main.lua
-- Author: Chan Ho Kwan
--
-----------------------------------------------------------------------------------------

-- hide the status bar
display.setStatusBar( display.HiddenStatusBar )

-- include the Corona "composer" module
local composer = require "composer"

-- load menu screen
composer.gotoScene( "start", "fade", 400 )