-----------------------------------------------------------------------------------------
--
-- File: StatusBar.lua
-- Author: Chan Ho Kwan
--
-----------------------------------------------------------------------------------------


local StatusBar = {}

function StatusBar:newGroup()
	local statusBar = display.newGroup()
	statusBar.anchorChildren = true
	statusBar.anchorX = 0
	statusBar.anchorY = 0
	statusBar.x = 10
	statusBar.y = 10
	
	function statusBar:newPlayerHpBar(playerMaxHp)
		local damageBar = display.newRect(0, 1, playerMaxHp/10, 29)
		damageBar.anchorX = 0
		damageBar.anchorY = 0
		damageBar:setFillColor(1, 0, 0)
		statusBar:insert(damageBar)
		
		local playerHpBar = display.newRect(0, 0, playerMaxHp/10, 30)
		playerHpBar.anchorX = 0
		playerHpBar.anchorY = 0
		playerHpBar:setFillColor(0, 1, 0)
		statusBar:insert(playerHpBar)
		
		return playerHpBar
	end
	
	function statusBar:newShieldPowerBar(shieldMaxPower)
		local shieldPowerBar = display.newRect(0, 30, shieldMaxPower/10, 30)
		shieldPowerBar.anchorX = 0
		shieldPowerBar.anchorY = 0
		shieldPowerBar:setFillColor(0, 0.5, 1)
		statusBar:insert(shieldPowerBar)
		
		return shieldPowerBar
	end
	
	return statusBar
end

return StatusBar