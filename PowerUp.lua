-----------------------------------------------------------------------------------------
--
-- File: PowerUp.lua
-- Author: Chan Ho Kwan
--
-----------------------------------------------------------------------------------------


local PowerUp = {}

function PowerUp:newGroup()
	local powerUpGroup = display.newGroup()
	powerUpGroup.anchorChildren = true
	powerUpGroup.anchorX = 0.5
	powerUpGroup.anchorY = 0.5
	powerUpGroup.x = 0
	powerUpGroup.y = 0
	
	function powerUpGroup:newPowerUp(imageDir, powerUpType, x, y, playerGroup)
		local image = nil
		if powerUpType == 0 then
			image = 'PowerUps/powerupRed_hp.png'
		elseif powerUpType == 1 then
			image = 'PowerUps/powerupBlue_shield.png'
		elseif powerUpType == 2 then
			image = 'PowerUps/powerupGreen_laser.png'
		end
		
		local powerUp = display.newImage(imageDir .. image, true)
		powerUp.anchorX = 0.5
		powerUp.anchorY = 0.5
		powerUp.x = x
		powerUp.y = y
		powerUp.speed = 10
		powerUp.powerUpType = powerUpType
		
		powerUp:addEventListener("collision", powerUp)
		
		self:insert(powerUp)
		
		function powerUp:collision(event)
			audio.play(playerGroup.powerUpSound)
			if self.powerUpType == 0 then
				if playerGroup[1].hp + 500 > playerGroup[1].maxHp then
					playerGroup[1].hp = playerGroup[1].maxHp
				else
					playerGroup[1].hp = playerGroup[1].hp + 500
				end
				transition.to(playerGroup.playerHpBar, {time=200, width=playerGroup[1].hp/10})
			elseif self.powerUpType == 1 then
				if playerGroup[2].power + 500 > playerGroup[2].maxPower then
					playerGroup[2].power = playerGroup[2].maxPower
				elseif playerGroup[2].power <= 0 then
					playerGroup.regenShieldTimer = timer.performWithDelay(200, regenerateShield, 1)
				else
					playerGroup[2].power = playerGroup[2].power + 500
				end
				transition.to(playerGroup.shieldPowerBar, {time=200, width=playerGroup[2].power/10})
			elseif self.powerUpType == 2 and playerGroup.laserLevel < 6 then
				playerGroup.laserLevel = playerGroup.laserLevel + 1
			end
			self:removeEventListener("collision", self)
			powerUpGroup:remove(self)
			display:remove(self)
			self:removeSelf()
			self = nil
		end
		
		function regenerateShield()
			playerGroup[2].power = playerGroup[2].power + 500
			playerGroup[2].isBodyActive = true
			playerGroup[1].isBodyActive = false
			playerGroup.regenShieldTransition = transition.to(playerGroup.shieldPowerBar, {time=200, width=playerGroup[2].power/10})
			playerGroup.messageString:setFillColor(0, 0.5, 0.5)
			playerGroup.messageString.text = "Shield is regenerated"
			playerGroup.messageString:fadeIn()
		end
		
		return powerUp
	end
	
	function powerUpGroup:setMovePowerUpTimer()
		local movePowerUps = function() return self:movePowerUps() end
		local movePowerUpTimer = timer.performWithDelay(2, movePowerUps, -1)
		
		return movePowerUpTimer
	end
	
	function powerUpGroup:movePowerUps()
		if self.numChildren and self.pause.isPaused == false then
			for a = self.numChildren,1,-1  do
				if(self[a].y < display.contentHeight + 100) then
					self[a].y = self[a].y + self[a].speed
				else
					local tmp = self[a]
					self.physics.removeBody(tmp)
					self:remove(tmp)
					display:remove(tmp)
				end	
			end
		end
	end
	
	return powerUpGroup
end

return PowerUp