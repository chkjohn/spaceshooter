-----------------------------------------------------------------------------------------
--
-- File: Enemy.lua
-- Author: Chan Ho Kwan
--
-----------------------------------------------------------------------------------------


local Enemy = {}

function Enemy:newGroup()
	local enemyGroup = display.newGroup()
	enemyGroup.anchorChildren = true
	enemyGroup.anchorX = 0.5
	enemyGroup.anchorY = 0.5
	enemyGroup.x = 0
	enemyGroup.y = 0
	
	function enemyGroup:newEnemy(imageDir, rand, enemyMaxSpeed, enemyMaxHp, enemyScore, level)
		local image, e_type
		if level < 4 then
			image = 'Enemies/enemyRed1.png'
			e_type = 1
		else
			image = 'Enemies/enemyBlue3.png'
			e_type = 2
		end
		
		local enemy = display.newImageRect(imageDir .. image, 93, 84)
		enemy.anchorX = 0.5
		enemy.anchorY = 0.5
		enemy.x = rand(display.contentCenterX - 200, display.contentCenterX + 200)
		enemy.y = -50
		enemy.initX = enemy.x
		enemy.amp = rand(50, 200)
		enemy.angle = rand(1,360)
		enemy.speed = rand(1, enemyMaxSpeed)
		enemy.hp = enemyMaxHp * e_type
		enemy.damage = enemyMaxHp * e_type
		enemy.score = enemyScore * e_type
		enemy.type = e_type
		
		enemy:addEventListener("collision", enemy)
		
		self:insert(enemy)
		
		function enemy:collision(event)
			if event.phase == "began" then
				self.hp = self.hp - event.other.damage
			end
			
			if self.hp <= 0 then
				self.mydata.score = self.mydata.score + self.score
				self.checkLevelScore()
				self.scoreString.text = string.format("%07d", self.mydata.score)
				self.startExplosion(self)
				if rand(0, self.powerUpChance) == 0 then
					self.addPowerUp(self.x, self.y)
				end
				self:removeEventListener("collision", self)
				timer.cancel(self.addBulletTimer)
				enemyGroup:remove(self)
				display:remove(self)
				self:removeSelf()
				self = nil
			end
		end
		
		return enemy
	end
	
	function enemyGroup:setMoveEnemyTimer()
		local moveEnemies = function() return self:moveEnemies() end
		local moveEnemyTimer = timer.performWithDelay(2, moveEnemies, -1)
		
		return moveEnemyTimer
	end
	
	function enemyGroup:moveEnemies()
		if self.numChildren and self.pause.isPaused == false then
			for a = self.numChildren,1,-1  do
				local tmp = self[a]
				if(tmp.y < display.contentHeight + 100) then
					tmp.y = tmp.y + tmp.speed
					if tmp.type == 2 then
						tmp.angle = tmp.angle + .1
						tmp.x = tmp.amp * math.sin(tmp.angle) + tmp.initX
					end
				else
					timer.cancel(tmp.addBulletTimer)
					self.physics.removeBody(tmp)
					self:remove(tmp)
					display:remove(tmp)
				end	
			end
		end
	end
	
	return enemyGroup
end

return Enemy