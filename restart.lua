-----------------------------------------------------------------------------------------
--
-- File: reloading.lua
-- Author: Chan Ho Kwan
--
-----------------------------------------------------------------------------------------


local composer = require ("composer")
local scene = composer.newScene()
local widget = require "widget"
local mydata = require( "mydata" )
local score = require( "score" )
local imageDir = "images/"

local buttonSound = audio.loadSound("SoundEffect/select01.wav")


function restartGame(event)
	audio.play(buttonSound)
	saveScore()
	composer.gotoScene("game", "fade", 400)
end

function backToStartPage(event)
	audio.play(buttonSound)
	saveScore()
	composer.gotoScene("start", "fade", 400)
end

function showStart()
	restartTransition = transition.to(restart,{time=200, alpha=1})
	startPageTransition = transition.to(startPage,{time=200, alpha=1})
	scoreTextTransition = transition.to(scoreText,{time=600, alpha=1})
	scoreTextTransition = transition.to(bestText,{time=600, alpha=1})
end

function showScore()
	scoreTransition = transition.to(scoreBg,{time=600, y=display.contentCenterY,onComplete=showStart})
	
end

function showGameOver()
	fadeTransition = transition.to(gameOver,{time=600, alpha=1,onComplete=showScore})
end

function loadScore()
	local prevScore = score.load()
	if prevScore ~= nil then
		if prevScore <= mydata.score then
			score.set(mydata.score)
		else 
			score.set(prevScore)	
		end
	else 
		score.set(mydata.score)	
		score.save()
	end
end

function saveScore()
	score.save()
end

function scene:create(event)

	local screenGroup = self.view

	local background = display.newImageRect(imageDir .. "starscape.png", 800, 1425)
	background.anchorX = 0.5
	background.anchorY = 0.5
	background.x = display.contentCenterX
	background.y = display.contentCenterY
	
	gameOver = display.newImageRect(imageDir .. "gameOver.png", 500, 100)
	gameOver.anchorX = 0.5
	gameOver.anchorY = 0.5
	gameOver.x = display.contentCenterX 
	gameOver.y = display.contentCenterY - 400
	gameOver.alpha = 0
	
	scoreBg = display.newImageRect(imageDir .. "menuBg.png", 480, 393)
	scoreBg.anchorX = 0.5
	scoreBg.anchorY = 0.5
    scoreBg.x = display.contentCenterX
    scoreBg.y = display.contentHeight + 500
	
	restart = widget.newButton{
		label="Restart",
		labelColor = { default={255}, over={128} },
		font=native.systemFontBold,
		fontSize=40,
		defaultFile=imageDir.."button.png",
		overFile=imageDir.."button-over.png",
		width=308, height=80,
		onRelease = restartGame	-- event listener function
	}
	restart.anchorX = 0.5
	restart.anchorY = 1
	restart.x = display.contentCenterX
	restart.y = display.contentCenterY + 350
	restart.alpha = 0
	
	startPage = widget.newButton{
		label="Start Page",
		labelColor = { default={255}, over={128} },
		font=native.systemFontBold,
		fontSize=40,
		defaultFile=imageDir.."button.png",
		overFile=imageDir.."button-over.png",
		width=308, height=80,
		onRelease = backToStartPage	-- event listener function
	}
	startPage.anchorX = 0.5
	startPage.anchorY = 1
	startPage.x = display.contentCenterX
	startPage.y = display.contentCenterY + 500
	startPage.alpha = 0
	
	local scoreString = string.format("%7d", mydata.score)
	scoreText = display.newText(scoreString, display.contentCenterX + 90, display.contentCenterY - 60, native.systemFontBold, 50, "right")
	scoreText:setFillColor(0,0,0)
	scoreText.alpha = 0 
		
	bestText = score.init({
		fontSize = 50,
		font = native.systemFontBold,
		x = display.contentCenterX + 90,
		y = display.contentCenterY + 85,
		align = "right",
		maxDigits = 7,
		leadingZeros = false,
		filename = "scorefile.txt",
	})
	bestScore = score.get()
	bestText.text = bestScore
	bestText.alpha = 0
	bestText:setFillColor(0,0,0)
	
	
	screenGroup:insert(background)
	screenGroup:insert(gameOver)
	screenGroup:insert(scoreBg)
	screenGroup:insert(restart)
	screenGroup:insert(startPage)
	screenGroup:insert(scoreText)
	screenGroup:insert(bestText)
	
end

function scene:show(event)
	if event.phase == "did" then
		composer.removeScene("game")
		composer.removeScene("start")
		
		showGameOver()
		loadScore()
	end
end

function scene:hide(event)
	if event.phase == "will" then
		transition.cancel(fadeTransition)
		transition.cancel(scoreTransition)
		transition.cancel(scoreTextTransition)
		transition.cancel(startTransition)
		
		--audio.dispose(buttonSound)
	end
end

function scene:destroy(event)

end


scene:addEventListener("create", scene)
scene:addEventListener("show", scene)
scene:addEventListener("hide", scene)
scene:addEventListener("destroy", scene)

return scene
