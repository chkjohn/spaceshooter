-----------------------------------------------------------------------------------------
--
-- File: Laser.lua
-- Author: Chan Ho Kwan
--
-----------------------------------------------------------------------------------------


local Laser = {}

function Laser:newGroup()
	local laserGroup = display.newGroup()
	laserGroup.anchorChildren = true
	laserGroup.anchorX = 0.5
	laserGroup.anchorY = 0.5
	laserGroup.x = 0
	laserGroup.y = 0
	
	function laserGroup:newLaser(x, y, laserPower, laserSpeed, imageDir, image)
		local laser = display.newImage(imageDir .. image, true)
		laser.anchorX = 0.5
		laser.anchorY = 1
		laser.x = x
		laser.y = y
		laser.damage = laserPower
		
		laser:addEventListener("collision", laser)
		
		self:insert(laser)
		
		function laser:collision(event)
			self:removeEventListener("collision", self)
			laserGroup:remove(self)
			display:remove(self)
			self:removeSelf()
			self = nil
		end
		
		return laser
	end
	
	function laserGroup:setCleanLaserTimer()
		local cleanLasers = function() return self:cleanLasers() end
		local cleanLaserTimer = timer.performWithDelay(20, cleanLasers, -1)
		
		return cleanLaserTimer
	end
	
	function laserGroup:cleanLasers()
		if self.numChildren then
			for a = self.numChildren,1,-1  do
				if(self[a].y <= -100) then
					local tmp = self[a]
					self.physics.removeBody(tmp)
					self:remove(tmp)
					display:remove(tmp)
				end	
			end
		end
	end
	
	return laserGroup
end

return Laser